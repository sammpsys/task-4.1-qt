# Task 4.1 QT
A repo consisting of two separate projects. One project is called Starsigns and returns a starsign based on user input. The other project is called Project2 and compares two words and returns the percentage of how many letters from the first word are found in the second word.

## Requirements

Requires `QTCreator`.

## Add your files


Clone the repo and open eacH respective project in QTCreator. Build and run the main.cpp file in each project. Make sure that you have enabled the projects to run in the terminal. This can be done by going into the "Projects" tab and navigating to Run and checking in the box, "Run in terminal". Project2 uses CMake and Starsigns uses QMake.

## Maintainers

[Sam Nassiri @sammpsys](https://gitlab.com/sammpsys)

## Contributors and License

Copyright 2022,

[Sam Nassiri @sammpsys](https://gitlab.com/sammpsys)

