#include <QCoreApplication>


#include <string>
#include <vector>
#include <algorithm>
#include <iostream>
#include<bits/stdc++.h>
#include <cstring>
#include <QTextStream>

QString word1;
QString word2;

float counter(QString w1, QString w2){
    QTextStream qout(stdout);
    int match=0;
    float fraction=0;
    int leng=w2.length();
    for (int i=0;i<w1.length();i++){
        for (int j=0;j<w2.length();j++){
            if(w1[i]==w2[j]){
                match++;
            }
        }
    }
    fraction=(float) match/leng;
    qout << "Match " << match << "\n";
    qout.flush();
    qout << "First word comprises " << fraction*100 << " % of second word" << "\n";
    return fraction*100;

}

//int main(){
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QTextStream qin(stdin);
    QTextStream qout(stdout);

    qout << "Enter first word " << "\n";
    qout.flush();
    word1= qin.readLine();
    qout << "Enter second word " << "\n";
    qout.flush();
    word2=  qin.readLine();

    counter(word1,word2);

    float result1=counter("sam","samuel");
    Q_ASSERT(result1==50);
    float result2=counter("dan","daniel");
    Q_ASSERT(result2==50);
    float result3=counter("h","hello");
    Q_ASSERT(result3==20);


    return a.exec();
}

