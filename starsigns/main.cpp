#include <QCoreApplication>

#include <QTextStream>
#include <typeinfo>
#include <iostream>

int date;
int month;
QString starsign;
QTextStream qin(stdin);
QTextStream qout(stdout);

QString checksign(int date, int month){

    if ( (month == 1 && date >=20 && date <=31) || (month == 2 && date >=1 && date <=18)  ){
        starsign="Aquarius";
        qout << "Your star sign is " << starsign << " as you were born on " << date << "/" << month << "\n";
        qout.flush();
        return starsign;
    }
    else if ( (month == 2 && date >=19 && date <=29) || (month == 3 && date >=1 && date <=20)  ){
        starsign="Pisces";
        qout << "Your star sign is " << starsign << " as you were born on " << date << "/" << month << "\n";
        qout.flush();
        return starsign;
    }
    else if ( (month == 3 && date >=21 && date <=31) || (month == 4 && date >=1 && date <=19)  ){
        starsign="Aries";
        qout << "Your star sign is " << starsign << " as you were born on " << date << "/" << month << "\n";
        qout.flush();
        return starsign;
    }
    else if ( (month == 4 && date >=20 && date <=30) || (month == 5 && date >=1 && date <=20)  ){
        starsign="Taurus";
        qout << "Your star sign is " << starsign << " as you were born on " << date << "/" << month << "\n";
        qout.flush();
        return starsign;
    }
    else if ( (month == 5 && date >=21 && date <=31) || (month == 6 && date >=1 && date <=21)  ){
        starsign="Gemini";
        qout << "Your star sign is " << starsign << " as you were born on " << date << "/" << month << "\n";
        qout.flush();
        return starsign;
    }
    else if ( (month == 6 && date >=22 && date <=30) || (month == 7 && date >=1 && date <=22)  ){
        starsign="Cancer";
        qout << "Your star sign is " << starsign << " as you were born on " << date << "/" << month << "\n";
        qout.flush();
        return starsign;
    }
    else if ( (month == 7 && date >=23 && date <=31) || (month == 8 && date >=1 && date <=22)  ){
        starsign="Leo";
        qout << "Your star sign is " << starsign << " as you were born on " << date << "/" << month << "\n";
        qout.flush();
        return starsign;
    }
    else if ( (month == 8 && date >=23 && date <=31) || (month == 9 && date >=1 && date <=22)  ){
        starsign="Virgo";
        qout << "Your star sign is " << starsign << " as you were born on " << date << "/" << month << "\n";
        qout.flush();
        return starsign;
    }
    else if ( (month == 9 && date >=23 && date <=30) || (month == 10 && date >=1 && date <=22)  ){
        starsign="Libra";
        qout << "Your star sign is " << starsign << " as you were born on " << date << "/" << month << "\n";
        qout.flush();
        return starsign;
    }
    else if ( (month == 10 && date >=23 && date <=31) || (month == 11 && date >=1 && date <=22)  ){
        starsign="Scorpio";
        qout << "Your star sign is " << starsign << " as you were born on " << date << "/" << month << "\n";
        qout.flush();
        return starsign;
    }
    else if ( (month == 11 && date >=23 && date <=30) || (month == 12 && date >=1 && date <=21)  ){
        starsign="Sagittarius";
        qout << "Your star sign is " << starsign << " as you were born on " << date << "/" << month << "\n";
        qout.flush();
        return starsign;
    }
    else if ( (month == 12 && date >=23 && date <=31) || (month == 1 && date >=1 && date <=19)  ){
        starsign="Capricorn";
        qout << "Your star sign is " << starsign << " as you were born on " << date << "/" << month << "\n";
        qout.flush();
        return starsign;
    }
    else {
        starsign="nothing";
        qout.flush();
        return starsign;
    }

}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QTextStream qin(stdin);
    QTextStream qout(stdout);

    qout << "What date in the month were you born?" << "\n";;
    qout.flush();
    int input1;
    std::cin >> input1;


    qout << "What month were you born?" << "\n";;
    qout.flush();

    int input2;
    std::cin >> input2;


    QString result=checksign(input1, input2);

    QString result1=checksign(16, 11);
    Q_ASSERT(result1=="Scorpio");
    QString result2=checksign(20, 2);
    Q_ASSERT(result2=="Pisces");
    QString result3=checksign(25, 3);
    Q_ASSERT(result3=="Aries");

    return a.exec();
}
